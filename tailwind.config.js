/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.html', 
    './webviews/**/*.html'
  ],
  theme: {
    extend: {}
  },
  plugins: []
}