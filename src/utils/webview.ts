import * as vscode from 'vscode'
import * as fs from 'fs'
// import * as path from 'path'
import util from './index'
import { showError } from './message'

function getLibStr(prefix: string, uri: string) {
  let str = ''
  let tag = ''
  if (prefix.startsWith('<link')) tag = 'style'
  else if (prefix.startsWith('<script')) tag = 'script'
  try {
    str = fs.readFileSync(uri, 'utf-8')
  } catch (error) {}
  return `
    <${tag}>
      ${str}
    </${tag}>
  `
}

/**
 * 从某个HTML文件读取能被Webview加载的HTML内容
 * @param {*} context 上下文
 * @param {*} templatePath 相对于插件根目录的html文件相对路径
 */
const getWebViewContent = (
  context: {
    extensionPath: string
    extensionUri: vscode.Uri
  },
  templatePath: string
) => {
  const resourcePath = util.getExtensionFileAbsolutePath(context, templatePath)
  // const dirPath = path.dirname(resourcePath)
  let html = fs.readFileSync(resourcePath, 'utf-8')
  // vscode不支持直接加载本地资源，需要替换成其专有路径格式，这里只是简单的将样式和JS的路径替换
  html = html.replace(
    /(<link.+?href="|<script.+?src="|<img.+?src=")(.+?)("\s*\/>|"><\/script>)/g,
    (_m, $1, $2) => getLibStr(
      $1,
      vscode.Uri.joinPath(context.extensionUri, 'libs', $2).fsPath
    )
    // return `${
    //   $1 +
    //   vscode.Uri.file(path.resolve(dirPath, $2))
    //     .with({ scheme: 'vscode-resource' })
    //     .toString()
    // }`
  )
  return html
}

/**
 * 执行回调函数
 *
 * @param {*} panel
 * @param {*} message
 * @param {*} resp
 */
const invokeCallback = (panel: any, message: any, resp: any) => {
  // console.log('回调消息：', resp)
  // 错误码在400-600之间弹出错误提示
  if (
    typeof resp == 'object' &&
    resp.code &&
    resp.code >= 400 &&
    resp.code < 600
  ) {
    showError(resp.message || '发生未知错误！')
  }
  // 传递响应数据和自定义回调方法
  panel.webview.postMessage({
    type: 'vscodeTTCallback',
    cbid: message.cbid,
    data: resp
  })
}

export { getWebViewContent, invokeCallback }
